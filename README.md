# patch-reapply-demo

A project which demonstrates how the same patch can be applied over and over to the same file.

To test, run

git am 0001-updated-file.patch
